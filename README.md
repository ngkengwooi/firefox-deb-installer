# firefox-deb-installer

An installation script to download the latest version of the Firefox browser for Linux, package it as a .deb file and install it using dpkg.